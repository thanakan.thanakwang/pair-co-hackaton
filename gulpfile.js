const gulp = require('gulp')
const handler = require('serve-handler')
const http = require('http')
const browserSync = require('browser-sync')
var preprocess = require("gulp-preprocess")

gulp.task('serve', () => {
    const server = http.createServer((request, response) => {
        return handler(request, response, {
            public: 'build'
        })
    })
    server.listen(4000)
    const bs = browserSync({
        proxy: 'localhost:4000',
        browser: ['google chrome', 'chromium']
    })
    const handleStop = () => {
        bs.cleanup()
        server.close()
    }
    process.on('SIGTERM', handleStop)
    process.on('SIGINT', handleStop)
})

gulp.task("copy-src", function() {
    gulp
        .src(['./src/**/*', '!./src/**/*.html'])
        .pipe(gulp.dest("./build/"))
        .pipe(browserSync.stream());
})

gulp.task("html", function() {
    gulp
        .src("./src/**/*.html")
        .pipe(preprocess())
        .pipe(gulp.dest("./build/"))
        .pipe(browserSync.stream());
})

gulp.task('watch', () => {
    gulp.watch('./src/**/*').on('change', gulp.parallel('copy-src', 'html'))
})

gulp.task('dev', gulp.parallel('serve', 'watch', 'copy-src', 'html'))




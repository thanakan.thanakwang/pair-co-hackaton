$(document).ready(function (events, handler) {
    var controller = new ScrollMagic.Controller()
    var fullPageHeight = $("#pinExpContent").height()
    function expPageTestAniamtion () {
        var timeline = new TimelineMax()
            .fromTo("#small-text", 1, {
                opacity: 0, 
                visibility: 'hidden',
                transform: `translate3d(0px, ${fullPageHeight}px, 0px)`,
            }, {
                opacity: 1, 
                visibility: 'visible',
                transform: `translate3d(0px, 0px, 0px)`,
            })
            // .fromTo("#big-text", 1, {
            //     opacity: 0, 
            //     visibility: 'hidden',
            //     transform: `translate3d(0px, ${fullPageHeight}px, 0px)`,
            // }, {
            //     opacity: 1, 
            //     visibility: 'visible',
            //     transform: `translate3d(0px, 0px, 0px)`,
            // })
            .fromTo("#big-text", 2, {
                transform: 'scale(0, 0)'
            }, {
                transform: 'scale(1, 1)'
            })
            .fromTo("#v-all", 1, {
                opacity: 0, 
                visibility: 'hidden',
                transform: `translate3d(0px, ${fullPageHeight}px, 0px)`,
            }, {
                opacity: 1, 
                visibility: 'visible',
                transform: `translate3d(0px, 0px, 0px)`,
            })
            .fromTo("#exp-images", 1, {
                opacity: 0, 
                visibility: 'hidden',
                transform: `translate3d(0px, ${fullPageHeight}px, 0px)`,
            }, {
                opacity: 1, 
                visibility: 'visible',
                transform: `translate3d(0px, 0px, 0px)`,
            })
    
        return new ScrollMagic.Scene({
            triggerElement: "#pinExpContent",
            triggerHook: "onLeave",
            duration: "150%",
            reverse: true
        })
        .setPin("#pinExpContent")
        .setTween(timeline)
        .addIndicators('EXP goes up')
        .addTo(controller);
    }
    expPageTestAniamtion() 
})


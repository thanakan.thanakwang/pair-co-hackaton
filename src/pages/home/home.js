$(document).ready(function (events, handler) {
    var controller = new ScrollMagic.Controller()

    function prepareLayOut () {
        const fullPageSelector = $('#pinContainer')
        const subTriangle = $('.triangle')
        const mainTriangle = $('.trapezoid')
        const fullHeight = (19 / 100) * fullPageSelector.height()
        subTriangle.css({
            'bottom': `${-(fullPageSelector.height() / 5)}px`
        })
        
        mainTriangle.css({
            'width': `${fullPageSelector.width()}px`,
            'height': `${(180 / 100) * fullPageSelector.height()}px`,
        })

    }

    function playTitleAnimation () {
        $('.text-title').each((i,el)=> {
            setTimeout(function() {
                $('.text-title').eq(i).addClass('fadeUp');
            }, i*1000); 
        })
        
       
    }

    //init
    playTitleAnimation()
    prepareLayOut()

    function logoToTheTop () {
        var distanceFromTop = document.getElementById("logo").offsetTop
        var calculateDistanceFromTop = distanceFromTop - 24
        var leftDistance = $('#home-content').offset().left / 2
        var targetDestination = `translate3d(${-leftDistance}px, ${-(calculateDistanceFromTop)}px, 45px)`
        var targeWidth = $("#logo").width()

        var timeline = new TimelineMax()
            .fromTo("#logo", 1, {
                transform:' translate3d(0px, 0px, 0px)', 
                width: targeWidth
            }, {
                transform: targetDestination, 
                ease: Linear.easeNone, width: (targeWidth / 2),
            })
        
        var resultScene = new ScrollMagic.Scene({
                triggerElement: "#pinContainer",
                triggerHook: "onLeave",
                duration: "150%",
                reverse: true
            })
            .setPin("#pinContainer")
            .setTween(timeline)
            .addIndicators('Logo to the top')

        return resultScene
    }

    var LogoScene = logoToTheTop()
    controller.addScene([
        LogoScene,
    ]);
})
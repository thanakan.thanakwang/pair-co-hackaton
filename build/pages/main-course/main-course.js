$(document).ready(function (events, handler) {
    var controller = new ScrollMagic.Controller()

    const fadeBeforeSlideObj = {
        opacity: 0, 
        visibility: 'hidden',
        transform:  `translate3d(0px, 24px, 0px)`
    }

    const fadeAfterSlideObj = {
        opacity:1, 
        visibility: 'visible',
        transform: `translate3d(0px, 0px, 0px)`,
        ease: Linear.ease
    }
    function fadeSlideALL () {
        var timeline = new TimelineMax()
            .fromTo("#main-course-title", 2, {...fadeBeforeSlideObj}, {...fadeAfterSlideObj})
            .fromTo("#main-course-second-title", 2, {...fadeBeforeSlideObj}, {...fadeAfterSlideObj})
            .fromTo(".main-course-sub-title", 2, {...fadeBeforeSlideObj}, {...fadeAfterSlideObj})
            .fromTo(".main-course-description", 2, {...fadeBeforeSlideObj}, {...fadeAfterSlideObj})
            .fromTo(".main-couse-button", 2, {...fadeBeforeSlideObj}, {...fadeAfterSlideObj})
            .fromTo("#main-course-img", 2, {...fadeBeforeSlideObj}, {...fadeAfterSlideObj})

        return new ScrollMagic.Scene({
            triggerElement: "#pinMainCourse",
            triggerHook: "onLeave",
            duration: "150%",
            reverse: true
        })
        .setPin("#pinMainCourse")
        .setTween(timeline)
        .addIndicators('Texts slide up')
        // .addTo(controller);
    }

    function fadeMainCourssContent () {
        const timeline = new TimelineMax()
            .fromTo(".more-content-title", 2, {...fadeBeforeSlideObj}, {...fadeAfterSlideObj})
            .fromTo(".more-content-button", 2, {...fadeBeforeSlideObj}, {...fadeAfterSlideObj})

       return new ScrollMagic.Scene({
            triggerElement: "#more-courses",
            triggerHook: "onCenter",
            duration: "150%",
            reverse: true
        })
            .setPin("#more-courses")
            .setTween(timeline)
            .addIndicators('More courses slide up')
    }

    var sectionMainCourse = fadeSlideALL()
    var sectionMoreCourses = fadeMainCourssContent()


    controller.addScene([
        sectionMainCourse,
        sectionMoreCourses
    ]);
})
$(document).ready(function (events, handler) {
    var controller = new ScrollMagic.Controller()
    var leftMost = $('#pinPairsContent').offset().left
    var centerXPos = parseInt($('.center-sign').css('padding-left'))

    function calculateLeftMost (selector, distance) {
        return -(distance + selector.width())
    }
    var theBeginOfImgLeft = calculateLeftMost($("#em-left"), leftMost)
    var theBeginOfImgRight = theBeginOfImgLeft * (-1)
    const fullPageSelector = $('#pinPairsContent')
    function pairsStartAnimation () {
        var timeline = new TimelineMax()
            .fromTo(".slide-pairs-text-up", 1, {
                opacity: 0, 
                visibility: 'hidden',
                transform: `translate3d(${centerXPos}px, ${fullPageSelector.height()}px, 0px)`,
            }, {
                opacity: 1, 
                visibility: 'visible',
                transform: `translate3d(${centerXPos}px, 0px, 0px)`,
            })
            .fromTo(".slide-pairs-text-right", 1, {
                opacity: 0, 
                visibility: 'hidden',
                transform: `translate3d(${fullPageSelector.width() / 2}px, 0px, 0px)`,
            }, {
                opacity: 1, 
                visibility: 'visible',
                transform: `translate3d(${centerXPos}px, 0px, 0px)`,
            })
            .fromTo(".pairs-fonts", 1, {
                transform: `translate3d(${centerXPos}px, 0px, 0px)`,
            }, {
                transform: `translate3d(0px, 0px, 0px)`,
            })
            .fromTo("#pairs-sub-title-top", 1, {
                transform: `translate3d(0px, ${fullPageSelector.height()}px, 0px)`,
            }, {
                transform: `translate3d(0px, 0px, 0px)`,
            })
            .fromTo("#em-left", 1, {
                transform: `translate3d(${theBeginOfImgLeft}px, 0px, 0px)`,
            }, {
                transform: `translate3d(0px, 0px, 0px)`,
            })
            .fromTo("#em-right", 1, {
                transform: `translate3d(${theBeginOfImgRight}px, 0px, 0px)`,
            }, {
                transform: `translate3d(0px, 0px, 0px)`,
            })
            .fromTo("#light", 1, {
                transform: `translate3d(0px, ${fullPageSelector.height()}px, 0px)`,
            }, {
                transform: `translate3d(0px, 0px, 0px)`,
            })
            .fromTo("#pairs-title-bottom", 1, {
                transform: `translate3d(0px, ${fullPageSelector.height()}px, 0px)`,
            }, {
                transform: `translate3d(0px, 0px, 0px)`,
            })
            
       new ScrollMagic.Scene({
            triggerElement: "#pinPairsContent",
            triggerHook: "onLeave",
            offset: 0,
            duration: '400%',
            reverse: true
        })
        .setPin("#pinPairsContent")
        .setTween(timeline)
        .addIndicators('Texts slide up')
        .addTo(controller);
    }
    pairsStartAnimation()
})